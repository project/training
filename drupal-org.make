api = "2"
core = "7.x"

;;;
; Projects
;;;
projects[admin_menu][version] = 3.0-rc5
projects[admin_menu][type] = module
projects[admin_menu][subdir] = contrib

projects[auto_nodetitle][version] = 1.0
projects[auto_nodetitle][type] = module
projects[auto_nodetitle][subdir] = contrib

projects[calendar][version] = 3.5
projects[calendar][type] = module
projects[calendar][subdir] = contrib

projects[ctools][version] = 1.12
projects[ctools][type] = module
projects[ctools][subdir] = contrib

projects[date][version] = 2.10
projects[date][type] = module
projects[date][subdir] = contrib

projects[entity][version] = 1.8
projects[entity][type] = module
projects[entity][subdir] = contrib

projects[features][version] = 2.10
projects[features][subdir] = contrib
projects[features][type] = module

projects[field_collection][version] = 1.0-beta12
projects[field_collection][subdir] = contrib
projects[field_collection][type] = module

projects[field_group][version] = 1.6
projects[field_group][subdir] = contrib
projects[field_group][type] = module

projects[field_permissions][version] = 1.0
projects[field_permissions][subdir] = contrib
projects[field_permissions][type] = module

projects[flowplayer][version] = 1.0-alpha1
projects[flowplayer][subdir] = contrib
projects[flowplayer][type] = module

projects[node_clone][version] = 1.0
projects[node_clone][subdir] = contrib
projects[node_clone][type] = module

projects[pathauto][version] = 1.3
projects[pathauto][subdir] = contrib
projects[pathauto][type] = module

projects[references][version] = 2.2
projects[references][subdir] = contrib
projects[references][type] = module

projects[strongarm][version] = 2.0
projects[strongarm][subdir] = contrib
projects[strongarm][type] = module

projects[token][version] = 1.7
projects[token][subdir] = contrib
projects[token][type] = module

projects[video][version] = 2.13
projects[video][subdir] = contrib
projects[video][type] = module

projects[views][version] = 3.18
projects[views][subdir] = contrib
projects[views][type] = module

projects[zen][version] = 5.6
projects[zen][type] = theme
